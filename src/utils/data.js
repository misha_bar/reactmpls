export const MONTHS = {
  allIds: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
  byId: {
    0: 'january',
    1: 'february',
    2: 'march',
    3: 'april',
    4: 'may',
    5: 'june',
    6: 'july',
    7: 'august',
    8: 'september',
    9: 'october',
    10: 'november',
    11: 'december'
  },
  byIdDisplay: {
    0: 'January',
    1: 'February',
    2: 'March',
    3: 'April',
    4: 'May',
    5: 'June',
    6: 'July',
    7: 'August',
    8: 'September',
    9: 'October',
    10: 'November',
    11: 'December'
  }
}
export const YEARS = ['2016', '2017']

export default { MONTHS, YEARS, arrayToObject, removeKey }

function arrayToObject(arr) {
  let newObj = {}
  arr.forEach(obj => newObj[obj.id] = { ...obj })
  return newObj
}

function removeKey(obj, deleteKey) {
  return Object.keys(obj)
    .filter(key => key !== deleteKey)
    .reduce((result, current) => {
      result[current] = obj[current]
      return result
  }, {})
}

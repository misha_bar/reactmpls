import { combineReducers } from 'redux'
import { reducer as toastrReducer } from 'react-redux-toastr'
import categories from './categories'
import user from './user'
import message from './message'
import todos from './todos'

const rootReducer = combineReducers({
  categories,
  message,
  todos,
  toastr: toastrReducer,
  user
})

export default rootReducer

import * as types from '../actions/types'
import initialState from './initial-state'
import dataUtils from '../utils/data'

export default function categoriesReducer(state = initialState.categories, action) {
  switch (action.type) {
    case types.CATEGORY_CREATE_START:
      return {
        ...state,
        allIds: [ ...state.allIds, 'creating' ],
        byId: {
          ...state.byId,
          'creating': action.category
        }
      }
    case types.CATEGORY_CREATE_SUCCESS:
      return {
        ...state,
        allIds: [ ...state.allIds.filter(id => id !== 'creating'), action.category.id ],
        byId: {
          ...dataUtils.removeKey(state.byId, 'creating'),
          [action.category.id]: action.category
        }
      }
    case types.CATEGORY_CREATE_FAIL:
      return {
        ...state,
        allIds: state.allIds.filter(id => id !== 'creating'),
        byId: dataUtils.removeKey(state.byId, 'creating')
      }
    case types.CATEGORY_DELETE_START:
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.category.id]: {
            ...state.byId[action.category.id],
            isDeleting: true
          }
        }
      }
    case types.CATEGORY_DELETE_SUCCESS:
      return {
        ...state,
        allIds: state.allIds.filter(id => id !== action.category.id),
        byId: dataUtils.removeKey(state.byId, action.category.id)
      }
    case types.CATEGORY_DELETE_FAIL:
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.category.id]: dataUtils.removeKey(state.byId[action.category.id], 'isDeleting')
        }
      }
    case types.CATEGORY_EDIT_START:
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.category.id]: {
            ...action.category,
            isEditing: true,
            old: { ...state.byId[action.category.id] }
          }
        }
      }
    case types.CATEGORY_EDIT_SUCCESS:
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.category.id]: {
            ...dataUtils.removeKey(state.byId[action.category.id], 'old'),
            isEditing: false
          }
        }
      }
    case types.CATEGORY_EDIT_FAIL:
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.category.id]: state.byId[action.category.id].old,
          isEditing: false
        }
      }
    case types.CATEGORIES_FETCH_START:
      return {
        ...state,
        isFetching: true
      }
    case types.CATEGORIES_FETCH_SUCCESS:
      return {
        ...state,
        isFetching: false,
        allIds: action.categories.map(category => category.id),
        byId: dataUtils.arrayToObject(action.categories)
      }
    case types.CATEGORIES_FETCH_FAIL:
      return {
        ...state,
        isFetching: false
      }
    default:
      return state
  }
}

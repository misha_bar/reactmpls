import * as types from './types'
import * as request from './request'
import { actions as toastrActions } from 'react-redux-toastr'

function createTodoStart(todo) {
  return {
    type: types.TODO_CREATE_START,
    todo
  }
}

function createTodoSuccess(todo) {
  return {
    type: types.TODO_CREATE_SUCCESS,
    todo
  }
}

function createTodoFail() {
  return {
    type: types.TODO_CREATE_FAIL
  }
}

function deleteTodoStart(todo) {
  return {
    type: types.TODO_DELETE_START,
    todo
  }
}

function deleteTodoSuccess(todo) {
  return {
    type: types.TODO_DELETE_SUCCESS,
    todo
  }
}

function deleteTodoFail(todo) {
  return {
    type: types.TODO_DELETE_FAIL,
    todo
  }
}

function editTodoStart(todo) {
  return {
    type: types.TODO_EDIT_START,
    todo
  }
}

function editTodoSuccess(todo) {
  return {
    type: types.TODO_EDIT_SUCCESS,
    todo
  }
}

function editTodoFail(todo) {
  return {
    type: types.TODO_EDIT_FAIL,
    todo
  }
}

function fetchTodosStart() {
  return {
    type: types.TODOS_FETCH_START
  }
}

function fetchTodosSuccess(todos) {
  return {
    type: types.TODOS_FETCH_SUCCESS,
    todos
  }
}

function fetchTodosFail() {
  return {
    type: types.TODOS_FETCH_FAIL
  }
}

export function createTodo(todo) {
  return (dispatch) => {
    dispatch(createTodoStart(todo))

    return dispatch(request.TODO_CREATE(todo))
      .then(response => {
        return response.json()
          .then(json => {
            if (!response.ok) {
              dispatch(createTodoFail())
              dispatch(toastrActions.error(json.message))
            } else {
              dispatch(createTodoSuccess(json))
              dispatch(toastrActions.success('Todo created!'))
            }

            return response
          })
      })
      .catch(error => request.HANDLE_ERROR_DEFAULT(error, dispatch, createTodoFail))
  }
}

export function deleteTodo(todo) {
  return (dispatch) => {
    dispatch(deleteTodoStart(todo))

    return dispatch(request.TODO_DELETE(todo.id))
      .then(response => {
        return response.json()
          .then(json => {
            if (!response.ok) {
              dispatch(deleteTodoFail())
              dispatch(toastrActions.error(json.message))
            } else {
              dispatch(deleteTodoSuccess(todo))
              dispatch(toastrActions.success(json.message))
            }

            return response
          })
      })
      .catch(error => request.HANDLE_ERROR_DEFAULT(error, dispatch))
      .catch(error => {
        dispatch(deleteTodoFail(todo))
        throw error
      })
  }
}

/**
 * Edit one category
 */
export function editTodo(todo) {
  return (dispatch) => {
    dispatch(editTodoStart(todo))

    return dispatch(request.TODO_PUT(todo))
      .then(response => {
        return response.json()
          .then(json => {
            if (!response.ok) {
              dispatch(editTodoFail(todo))
              dispatch(toastrActions.error(json.message))
            } else {
              dispatch(editTodoSuccess(json))
              dispatch(toastrActions.success('Todo edited'))
            }

            return response
          })
      })
      .catch(error => request.HANDLE_ERROR_DEFAULT(error, dispatch))
      .catch(error => {
        dispatch(editTodoFail(todo))
        throw error
      })
  }
}

export function fetchTodos() {
  return (dispatch, getState) => {
    const state = getState().todos
    if (state.isFetching || state.allIds.length !== 0) return

    dispatch(fetchTodosStart())

    return dispatch(request.TODOS_GET_ALL())
      .then(response => {
        return response.json()
          .then(json => {
            if (!response.ok) {
              dispatch(fetchTodosFail())
              dispatch(toastrActions.error(json.message))
            } else {
              dispatch(fetchTodosSuccess(json))
            }

            return response
          })
      })
      .catch(error => request.HANDLE_ERROR_DEFAULT(error, dispatch, fetchTodosFail))
  }
}

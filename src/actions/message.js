import * as types from './types'

/**
 * Valid style types: success, warning, danger, info
 */
export function addMessage(text, style = 'info', icon) {
  return {
    type: types.MESSAGE_ADD,
    text,
    style,
    icon
  }
}

export function removeMessage() {
  return {
    type: types.MESSAGE_REMOVE
  }
}

export function displayNetworkError() {
  return addMessage('A network error has occurred', 'danger', 'fa fa-exclamation-triangle')
}

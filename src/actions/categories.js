import * as types from './types'
import * as request from './request'
import { actions as toastrActions } from 'react-redux-toastr'

function createCategoryStart(category) {
  return {
    type: types.CATEGORY_CREATE_START,
    category
  }
}

function createCategorySuccess(category) {
  return {
    type: types.CATEGORY_CREATE_SUCCESS,
    category
  }
}

function createCategoryFail() {
  return {
    type: types.CATEGORY_CREATE_FAIL
  }
}

function deleteCategoryStart(category) {
  return {
    type: types.CATEGORY_DELETE_START,
    category
  }
}

function deleteCategorySuccess(category) {
  return {
    type: types.CATEGORY_DELETE_SUCCESS,
    category
  }
}

function deleteCategoryFail(category) {
  return {
    type: types.CATEGORY_DELETE_FAIL,
    category
  }
}

function editCategoryStart(category) {
  return {
    type: types.CATEGORY_EDIT_START,
    category
  }
}

function editCategorySuccess(category) {
  return {
    type: types.CATEGORY_EDIT_SUCCESS,
    category
  }
}

function editCategoryFail(category) {
  return {
    type: types.CATEGORY_EDIT_FAIL,
    category
  }
}

function fetchCategoriesStart() {
  return {
    type: types.CATEGORIES_FETCH_START
  }
}

function fetchCategoriesSuccess(categories) {
  return {
    type: types.CATEGORIES_FETCH_SUCCESS,
    categories
  }
}

function fetchCategoriesFail() {
  return {
    type: types.CATEGORIES_FETCH_FAIL
  }
}

/**
 * Create a new category
 */
export function createCategory(category) {
  return (dispatch) => {
    dispatch(createCategoryStart(category))

    return dispatch(request.CATEGORY_CREATE(category))
      .then(response => {
        return response.json()
          .then(json => {
            if (!response.ok) {
              dispatch(createCategoryFail())
              dispatch(toastrActions.error(json.message))
            } else {
              dispatch(createCategorySuccess(json))
              dispatch(toastrActions.success('Category created!'))
            }

            return response
          })
      })
      .catch(error => request.HANDLE_ERROR_DEFAULT(error, dispatch, createCategoryFail))
  }
}

/**
 * Delete an existing category
 */
export function deleteCategory(category) {
  return (dispatch) => {
    dispatch(deleteCategoryStart(category))

    return dispatch(request.CATEGORY_DELETE(category.id))
      .then(response => {
        return response.json()
          .then(json => {
            if (!response.ok) {
              dispatch(deleteCategoryFail(category))
              dispatch(toastrActions.error(json.message))
            } else {
              dispatch(deleteCategorySuccess(category))
              dispatch(toastrActions.success(json.message))
            }

            return response
          })
      })
      .catch(error => request.HANDLE_ERROR_DEFAULT(error, dispatch))
      .catch(error => {
        dispatch(deleteCategoryFail(category))
        throw error
      })
  }
}

/**
 * Edit one category
 */
export function editCategory(category) {
  return (dispatch) => {
    dispatch(editCategoryStart(category))

    return dispatch(request.CATEGORY_PUT(category))
      .then(response => {
        return response.json()
          .then(json => {
            if (!response.ok) {
              dispatch(editCategoryFail(category))
              dispatch(toastrActions.error(json.message))
            } else {
              dispatch(editCategorySuccess(json))
              dispatch(toastrActions.success('Category name changed'))
            }

            return response
          })
      })
      .catch(error => request.HANDLE_ERROR_DEFAULT(error, dispatch))
      .catch(error => {
        dispatch(editCategoryFail(category))
        throw error
      })
  }
}

/**
 * Fetch all category data
 */
export function fetchCategories() {
  return (dispatch, getState) => {
    const state = getState().categories
    if (state.isFetching || state.allIds.length !== 0) return
    
    dispatch(fetchCategoriesStart())
    
    return dispatch(request.CATEGORIES_GET_ALL())
      .then(response => {
        return response.json()
          .then(json => {
            if (!response.ok) {
              dispatch(fetchCategoriesFail())
              dispatch(toastrActions.error(json.message))
            } else {
              dispatch(fetchCategoriesSuccess(json))
            }

            return response
          })
      })
      .catch(error => request.HANDLE_ERROR_DEFAULT(error, dispatch, fetchCategoriesFail))
  }
}

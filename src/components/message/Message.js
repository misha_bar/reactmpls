import React, { Component, PropTypes } from 'react'
import { Alert } from 'react-bootstrap'

class Message extends Component {
  render() {
    return (
      <Alert bsStyle={this.props.style}>
        { this.props.icon ? (<i className={this.props.icon}></i>) : null } {this.props.children}
      </Alert>
    )
  }
}

Message.propTypes = {
  style: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired
}

export default Message

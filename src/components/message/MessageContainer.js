import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { browserHistory } from 'react-router'
import * as messageActions from '../../actions/message'
import Message from './Message'

class MessageContainer extends Component {
  constructor(props, context) {
    super(props, context)

    this.close = this.close.bind(this)
  }

  componentDidMount() {
    let justLoaded = true
    browserHistory.listen((e) => {
      if (!justLoaded) return this.props.actions.removeMessage()

      justLoaded = false
    })
  }

  close() {
    return this.props.actions.removeMessage()
  }

  render() {
    return (
      <Message style={this.props.message.style} text={this.props.message.text} icon={this.props.message.icon}>
        {this.props.message.text}
      </Message>
    )
  }
}

MessageContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  message: PropTypes.object.isRequired
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(messageActions, dispatch)
  }
}

export default connect(null, mapDispatchToProps)(MessageContainer)

import React, { Component } from 'react'

class IndexPage extends Component {
  render() {
    return (
      <div>
        <p>
          Welcome the React Minneapolis!
          This application showcases the differences between creating applications in React and Angular. You're in the React application.
        </p>
      </div>
    )
  }
}

export default IndexPage

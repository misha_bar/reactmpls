import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ListGroup } from 'react-bootstrap'
import NewTodoItem from './NewTodoItem'
import TodoItem from './TodoItem'
import * as todosActions from '../../actions/todos'
import './TodoItem.css'

class TodosList extends Component {
  constructor(props) {
    super(props)

    this.handleRemove = this.handleRemove.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleToggleComplete = this.handleToggleComplete.bind(this)
  }

  handleRemove(id) {
    this.props.actions.deleteTodo(this.props.todos.byId[id])
  }

  handleSubmit(todo) {
    this.props.actions.createTodo(todo)
  }

  handleToggleComplete(id, completed) {
    this.props.actions.editTodo({
      ...this.props.todos.byId[id],
      completed
    })
  }

  componentWillMount() {
    this.props.actions.fetchTodos()
  }
  
  render() {
    return (
      <ListGroup>
        <NewTodoItem
          create={this.handleSubmit}
          />
        {this.props.todos.allIds.map(id =>
          <TodoItem
            key={id}
            todo={this.props.todos.byId[id]}
            toggleComplete={completed => this.handleToggleComplete(id, completed)}
            remove={() => this.handleRemove(id)}
            />
        )}
        { this.props.todos.isFetching ? (
          <div className="text-center">
            <i className="fa fa-spinner fa-pulse fa-fw fa-3x"></i>
          </div>
        ) : null}
      </ListGroup>
    )
  }
}

TodosList.propTypes = {
  actions: PropTypes.object.isRequired,
  todos: PropTypes.object.isRequired
}

function mapStateToProps(state, ownProps) {
  return {
    todos: state.todos
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(todosActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodosList)

import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ControlLabel, Form, FormControl, FormGroup } from 'react-bootstrap'
import * as todosActions from '../../actions/todos'

class NewTodoItem extends Component {
  constructor(props) {
    super(props)

    this.state = {
      title: '',
      description: '',
      isCreating: false
    }

    this.handleChange = this.handleChange.bind(this)
    this.onKeyUp = this.onKeyUp.bind(this)
  }

  handleChange(e) {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value
    })
  }

  handleSubmit() {
    const prevState = { ...this.state }

    this.setState({
      ...this.state,
      title: '',
      description: '',
      isCreating: true
    })

    this.props.actions.createTodo(this.state)
      .then(response => !response.ok ? this.setState({ ...prevState, isCreating: false}) : this.setState({ ...this.state, isCreating: false }))
      .catch(response => this.setState({ ...prevState, isCreating: false }))
  }

  onKeyUp(e) {
    e.preventDefault()

    if (e.keyCode === 13) this.handleSubmit()
  }

  render() {
    return (
      <Form
        className="newTodoItem"
        inline>
        <FormGroup controlId="todoformInlineTitle">
          <ControlLabel>Title</ControlLabel>
          {' '}
          <FormControl
            name="title"
            type="text"
            placeholder="Title"
            disabled={this.state.isCreating}
            onChange={this.handleChange}
            onKeyUp={this.onKeyUp}
            value={this.state.title} />
        </FormGroup>
        {' '}
        <FormGroup controlId="todoformInlineDescription">
          <ControlLabel>Description</ControlLabel>
          {' '}
          <FormControl
            name="description"
            type="text"
            placeholder="Description"
            disabled={this.state.isCreating}
            onChange={this.handleChange}
            onKeyUp={this.onKeyUp}
            value={this.state.description} />
        </FormGroup>
      </Form>
    )
  }
}

NewTodoItem.propTypes = {
  actions: PropTypes.object.isRequired,
  create: PropTypes.func.isRequired
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(todosActions, dispatch)
  }
}

export default connect(null, mapDispatchToProps)(NewTodoItem)

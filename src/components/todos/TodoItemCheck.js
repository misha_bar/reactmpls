import React, { PropTypes } from 'react'

const TodoItemCheck = ({ completed, toggleComplete }) => {
  return (
    <a href="#" onClick={e => {
      e.preventDefault()
      toggleComplete(!completed)
    }}>
      <i className={ completed ? 'fa fa-check-square-o fa-2x todoItemCheckComplete' : 'fa fa-square-o fa-2x todoItemCheckIncomplete' }></i>
    </a>
  )
}

TodoItemCheck.defaultProps = {
  completed: false
}

TodoItemCheck.propTypes = {
  completed: PropTypes.bool.isRequired,
  toggleComplete: PropTypes.func.isRequired
}

export default TodoItemCheck

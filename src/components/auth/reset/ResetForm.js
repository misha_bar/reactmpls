import React , { PropTypes } from 'react'
import { Form, FormGroup, Col, FormControl, ControlLabel } from 'react-bootstrap'
import DynamicSubmitButton from '../../form/DynamicSubmitButton'

const ResetForm = ({ form, handleChange, handleSubmit, submitDisabled }) => {
  return (
    <Form horizontal onSubmit={handleSubmit}>
      <FormGroup controlId="formHorizontalPassword">
        <Col componentClass={ControlLabel} sm={2}>
          New Password
        </Col>
        <Col sm={10}>
          <FormControl
            name="password"
            type="password"
            placeholder="Password"
            onChange={handleChange}
            value={form.password} />
        </Col>
      </FormGroup>

      <FormGroup>
        <Col smOffset={2} sm={10}>
          <DynamicSubmitButton
            disabled={submitDisabled}
            disabledText="Changing password..."
            text="Change password"
          />
        </Col>
      </FormGroup>
    </Form>
  )
}

ResetForm.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitDisabled: PropTypes.bool
}

export default ResetForm

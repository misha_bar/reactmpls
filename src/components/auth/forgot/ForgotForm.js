import React , { PropTypes } from 'react'
import { Form, FormGroup, Col, FormControl, ControlLabel } from 'react-bootstrap'
import DynamicSubmitButton from '../../form/DynamicSubmitButton'

const ForgotForm = ({ form, handleChange, handleSubmit, submitDisabled }) => {
  return (
    <Form horizontal onSubmit={handleSubmit}>
      <FormGroup controlId="formHorizontalEmail">
        <Col componentClass={ControlLabel} sm={2}>
          Email
        </Col>
        <Col sm={10}>
          <FormControl
            name="email"
            type="email"
            placeholder="Email"
            onChange={handleChange}
            value={form.email} />
        </Col>
      </FormGroup>

      <FormGroup>
        <Col smOffset={2} sm={10}>
          <DynamicSubmitButton
            disabled={submitDisabled}
            disabledText="Sending..."
            text="Submit"
          />
        </Col>
      </FormGroup>
    </Form>
  )
}

ForgotForm.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitDisabled: PropTypes.bool
}

export default ForgotForm

import React , { PropTypes } from 'react'
import { Form, FormGroup, Col, FormControl, ControlLabel } from 'react-bootstrap'
import DynamicSubmitButton from '../../form/DynamicSubmitButton'

const LoginForm = ({ form, handleChange, handleSubmit, submitDisabled }) => {
  return (
    <Form horizontal onSubmit={handleSubmit}>
      <FormGroup controlId="formHorizontalEmail">
        <Col componentClass={ControlLabel} sm={2}>
          Email
        </Col>
        <Col sm={10}>
          <FormControl
            name="email"
            type="email"
            placeholder="Email"
            onChange={handleChange}
            value={form.email} />
        </Col>
      </FormGroup>

      <FormGroup controlId="formHorizontalPassword">
        <Col componentClass={ControlLabel} sm={2}>
          Password
        </Col>
        <Col sm={10}>
          <FormControl
            name="password"
            type="password"
            placeholder="Password"
            onChange={handleChange}
            value={form.password} />
        </Col>
      </FormGroup>

      <FormGroup>
        <Col smOffset={2} sm={10}>
          <DynamicSubmitButton
            disabled={submitDisabled}
            disabledText="Creating account..."
            text="Register"
          />
        </Col>
      </FormGroup>
    </Form>
  )
}

LoginForm.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitDisabled: PropTypes.bool
}

export default LoginForm

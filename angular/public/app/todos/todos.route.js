(function() {
  'use strict';
  
  angular
    .module('app.todos')
    .run(run);
  
  run.$inject = ['routerHelper'];
  /* @ngInject */
  function run(routerHelper) {
    routerHelper.configureStates(getStates());
  }
  
  function getStates() {
    return [
      {
        state: 'todos',
        config: {
          url: '/todos',
          templateUrl: 'app/todos/todos.html',
          controller: 'TodosCtrl',
          controllerAs: 'tc',
          data: {
            requiresAuth: true
          }
        }
      }
    ];
  }
})();

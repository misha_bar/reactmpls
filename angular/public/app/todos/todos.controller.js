(function() {
  'use strict';

  angular
    .module('app.todos')
    .controller('TodosCtrl', TodosCtrl);

  TodosCtrl.$inject = ['todosService'];
  /* @ngInject */
  function TodosCtrl(todosService) {
    var vm = this;

    vm.createTodo = createTodo;
    vm.deleteTodo = todosService.deleteTodo;
    vm.toggleComplete = toggleComplete;
    vm.newTodo = { title: '', description: '' };
    vm.todos = todosService.todos;
    
    todosService.fetchTodos();

    function createTodo() {
      todosService.createTodo(vm.newTodo).catch(function(todo) {
        vm.newTodo = todo;
      });
      vm.newTodo = { title: '', description: '' };
    }

    function toggleComplete(todo) {
      todo.completed = !todo.completed;
      todosService.editTodo(todo);
    }
  }
})();

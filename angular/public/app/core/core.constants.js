(function() {
  'use strict';
  
  angular
    .module('app.core')
    .constant('API', API());
    
    function API() {
      var api = '/api/v0'; // ideally you should use gulp to build this var based on environment

      return {
        AUTH: api + '/auth',
        TODOS: api + '/todos'
      };
    }
})();

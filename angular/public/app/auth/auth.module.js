(function() {
  'use strict';

  angular.module('app.auth', [
    'app.core',
    
    /* 3rd-party modules */
    'ui.bootstrap'
  ]);
})();

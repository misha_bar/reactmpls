import { User } from '../db/models'
import { NotAuthorizedError } from '../errors/auth-errors'

export default {
  get,
  // getAll,
  // create,
  // update,
  // remove
  retrieve
}

/**
 * Get a single user
 */
function get(req, res, next) {
  return res.status(200).json(req.store.user)
}

/**
 * Validate and retrieve a user from the db
 */
function retrieve(req, res, next) {
  req.checkParams('email', 'Query must contain a valid user').notEmpty().isEmail()
  const errors = req.validationErrors()
  if (errors) return next(errors)

  if (req.params.email !== req.user.email) return next(new NotAuthorizedError())

  User.get(req.params.email).pluck('email', 'createdAt', 'updatedAt').getJoin().execute()
    .then(user => {
      req.store = { ...req.store, user }
      return next()
    })
    .catch(err => next(err))
}

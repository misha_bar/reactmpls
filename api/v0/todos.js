import { Category, Todo } from '../db/models'
import { DocumentNotFoundError } from '../errors/data-errors'

export default {
  create,
  get,
  getAll,
  remove,
  retrieve,
  update,
  validateId
}

/**
 * Create a new todo
 */
function create(req, res, next) {
  Todo.save({
    date: req.body.date,
    title: req.body.title,
    description: req.body.description,
    userEmail: req.user.email,
    categoryId: req.store.category ? req.store.category.id : undefined
  })
    .then(todo => res.status(200).json(todo))
    .catch(err => next(err))
}

/**
 * Respond with todo matching id
 */
function get(req, res, next) {
  return res.status(200).json(req.store.todo)
}

/**
 * Respond with all todos
 */
function getAll(req, res, next) {
  return res.status(200).json(req.store.todos)
}

/**
 * Delete the document from the DB
 */
function remove(req, res, next) {
  req.store.todo.delete()
    .then(result => res.status(200).json({ message: 'Todo deleted' }))
    .catch(err => next(err))
}

/**
 * Retrieve all todos for the authorized user
 */
function retrieve(req, res, next) {
  let params = { userEmail: req.user.email }
  if (req.params.todoId) params.id = req.params.todoId
  if (req.params.categoryId) params.categoryId = req.params.categoryId

  Todo.filter({ ...params })
    .then(todos => {
      if (params.id && !todos[0]) {
        throw new DocumentNotFoundError()
      } else if (params.id) {
        req.store = { ...req.store, todo: todos[0] }
      } else {
        req.store = { ...req.store, todos }
      }
      
      next()
    })
    .catch(err => next(err))
}

/**
 * Update a single todo
 * First validate that if there's a category for a given categoryId,
 * or ignore if a categoryId is not given
 */
function update(req, res, next) {
  new Promise((resolve, reject) => resolve())
    .then(() => {
      if (!req.body.categoryId || req.body.categoryId === '') return Promise.resolve(undefined)

      let params = { userEmail: req.user.email }
      params.id = req.body.categoryId
      return Category.filter({ ...params })
    })
    .then(categories => {
      if (!categories) return req.store.todo.merge(req.body).save()
      if (!categories[0]) throw new DocumentNotFoundError('Category is invalid')
      return req.store.todo.merge(req.body).save()
    })
    .then(todo => res.status(200).json(todo))
    .catch(err => next(err))
}

/**
 * Validate todo ID
 */
function validateId(req, res, next) {
  req.checkParams('todoId', 'Query must contain a valid todo id').notEmpty().isUUID(4)
  const errors = req.validationErrors()
  if (errors) return next(errors)
  
  return next()
}

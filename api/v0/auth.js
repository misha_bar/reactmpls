import { RefreshToken, User } from '../db/models'
import urlHelper from '../utils/url'
import * as authHeader from '../utils/auth-header'
import nodemailer from '../utils/mail'
import jwt from 'jsonwebtoken'
import { JWT_EXP, JWT_SECRET } from '../utils/session'
import { AccountDoesNotExistError, AccountNotVerifiedError,
  WrongPasswordError, AccountAlreadyExistsError,
  AccountAlreadyVerifiedError, InvalidPasswordResetTokenError,
  InvalidEmailVerificationTokenError, AuthorizationHeaderFormatError,
  RefreshTokenExpiredError, InvalidRefreshTokenError }
  from '../errors/auth-errors'

export default {
  forgotPassword,
  login,
  logout,
  logoutAll,
  resendVerification,
  refresh,
  register,
  resetPassword,
  validateEmail,
  validatePassword,
  verifyAccessToken,
  verifyEmailToken,
  verifyRefreshToken
}

/**
 * Send an e-mail to the account with a generated password reset token.
 */
function forgotPassword(req, res, next) {
  User.get(req.body.email)
    .then(user => user.createPasswordResetToken())
    .then(user => {
      nodemailer.sendMail({
        to: user.email,
        subject: 'NERRD-Starter | Password reset',
        text: 'You can change your password by clicking on the following link: '
          + urlHelper.getUrl(req)
          + '/reset?token=' + user.passwordResetToken
          + '&email=' + user.email
      })

      return res.status(200).json({
        message: 'Password reset link sent to your e-mail.'
      })
    })
    .catch(err => {
      if (err.name === 'DocumentNotFoundError') throw new AccountDoesNotExistError()
      throw err
    })
    .catch(err => next(err))
}

/**
 * User signin with username and password.
 * If successful, sign a JWT and create an opaque RefreshToken to send to requestor.
 */
function login(req, res, next) {
  req.checkBody('password', 'Password cannot be empty').notEmpty()
  const errors = req.validationErrors()
  if (errors) return next(errors)
  
  User.get(req.body.email)
    .then(user => {
      if (!user.isVerified) throw new AccountNotVerifiedError()
      
      return user.comparePassword(req.body.password)
        .then(passwordMatches => {
          if (!passwordMatches) throw new WrongPasswordError()

          return user
        })
    })
    .then(user => {
      const accessToken = jwt.sign({ email: user.email }, JWT_SECRET, { expiresIn: JWT_EXP })
      const newRefreshToken = new RefreshToken({ user: user })

      return newRefreshToken.saveAll().then((refreshToken) => { return { accessToken, refreshToken }})
    })
    .then(tokens => {
      return res.status(200).json({
        access_token: tokens.accessToken,
        refresh_token: tokens.refreshToken.token
      })
    })
    .catch(err => {
      if (err.name === 'DocumentNotFoundError') throw new AccountDoesNotExistError()
      throw err
    })
    .catch(err => next(err))
}

/**
 * Logout of the passed refresh token
 */
function logout(req, res, next) {
  req.refreshToken.logout()
    .then(() => res.status(200).end())
    .catch(err => next(err))
}

/**
 * Logout of all active refresh tokens for a user
 */
function logoutAll(req, res, next) {
  User.get(req.refreshToken.userEmail).getJoin({ refreshTokens: true })
    .then(user => {
      user.refreshTokens.forEach((refreshToken) => {
        if (!refreshToken.isExpired()) refreshToken.logout()
      })

      return res.status(200).end()
    })
    .catch(err => next(err))
}

/**
 * Issues a new access token using a refresh token.
 * Also extends the refresh token.
 */
function refresh(req, res, next) {
  req.refreshToken.extendExpiration()
    .then(refreshToken => {
      const access_token = jwt.sign({ email: req.refreshToken.userEmail }, JWT_SECRET, { expiresIn: JWT_EXP })
      return res.status(200).json({ access_token })
    })
    .catch(err => next(err))
}

/**
 * Register a new account.
 * If successful, send an e-mail verification token to the registered e-mail.
 * validateEmail() middleware should be run before this function.
 */
function register(req, res, next) {
  User.getAll(req.body.email)
    .then(users => {
      if (users.length > 0) throw new AccountAlreadyExistsError()
      
      return User.hashPassword(req.body.password)
    })
    .then(hash => User.save({ email: req.body.email, password: hash }))
    .then(user => {
      nodemailer.sendMail({
        to: user.email,
        subject: 'NERRD-Starter | Account Registration',
        text: 'Please verify your e-mail by clicking the following link: '
          + urlHelper.getUrl(req)
          + '/verify?verificationToken=' + user.verificationToken
          + '&email=' + user.email
      })
      
      return res.status(200).json({
        message: user.email
          + ' has been registered. Please check your e-mail for next steps.'
      })
    })
    .catch(err => next(err))
}

/**
 * Resend the e-mail verification token to the specified e-mail.
 * If account is already verified, ignore the request.
 */
function resendVerification(req, res, next) {
  User.get(req.body.email)
    .then(user => {
      if (user.isVerified) throw new AccountAlreadyVerifiedError()
      
      nodemailer.sendMail({
        to: user.email,
        subject: 'NERRD-Starter | Account Registration',
        text: 'Please verify your e-mail by clicking the following link: '
          + urlHelper.getUrl(req)
          + '/verify?verificationToken=' + user.verificationToken
          + '&email=' + user.email
      })

      return res.status(200).json({
        message: 'Verification e-mail has been sent. Please check your e-mail for next steps.'
      })
    })
    .catch(err => {
      if (err.name === 'DocumentNotFoundError') throw new AccountDoesNotExistError()
      throw err
    })
    .catch(err => next(err))
}

/**
 * Change the account's password if the given token matches and not expired.
 * Logout of all refresh tokens if successful in changing password.
 */
function resetPassword(req, res, next) {
  req.checkBody('token', 'Password reset token is invalid').notEmpty().isUUID(4)
  const errors = req.validationErrors()
  if (errors) return next(errors)
  
  User.get(req.body.email).getJoin({ refreshToken: true })
    .then(user => {
      if (!user.isValidPasswordResetToken(req.body.token)) throw new InvalidPasswordResetTokenError()

      return User.hashPassword(req.body.password)
        .then(hash => {
          user.password = hash
          return user
        })
    })
    .then(user => {
      user.passwordResetToken = null
      user.passwordResetExpiration = null
      return user.save()
    })
    .then(user => {
      user.refreshTokens.forEach((refreshToken) => {
        if (!refreshToken.isExpired()) refreshToken.logout()
      })

      return res.status(200).json({
        message: 'Password changed. Please re-login to continue.'
      })
    })
    .catch(err => {
      if (err.name === 'DocumentNotFoundError') throw new AccountDoesNotExistError()
      throw err
    })
    .catch(err => next(err))
}

/**
 * Validation middleware
 * Validate e-mail field in the request body.
 */
function validateEmail(req, res, next) {
  req.checkBody({
    email: {
      notEmpty: {
        errorMessage: 'E-mail cannot be empty.'
      },
      isEmail: {
        errorMessage: 'E-mail is invalid.'
      }
    }
  })
  const errors = req.validationErrors()
  if (errors) return next(errors)

  req.sanitizeBody('email').normalizeEmail()
  return next()
}

/**
 * Validation middleware
 * Validate password integrity and length in the request body.
 */
function validatePassword(req, res, next) {
  req.checkBody({
    password: {
      isLength: {
        options: [{ min: 8, max: 64 }],
        errorMessage: 'Password must be at least 8 characters in length.'
      }
    }
  })
  const errors = req.validationErrors()
  if (errors) return next(errors)
  
  return next()
}

/**
 * Checks if the incoming request contains a valid e-mail verification token.
 * If so, set the user as verified.
 */
function verifyEmailToken(req, res, next) {
  req.checkBody('verificationToken', 'Verification token is invalid').notEmpty().isUUID(4)
  const errors = req.validationErrors()
  if (errors) return next(errors)
  
  User.get(req.body.email)
    .then(user => {
      if (user.verificationToken !== req.body.verificationToken) throw new InvalidEmailVerificationTokenError()

      return user.verifyEmail()
    })
    .then(user => res.status(200).json({ message: user.email + ' has been verified. Please login to continue.' }))
    .catch(err => {
      if (err.name === 'DocumentNotFoundError') throw new AccountDoesNotExistError()
      throw err
    })
    .catch(err => next(err))
}

/**
 * Middleware
 * Checks if the incoming request contains a valid access token.
 * If valid, send them to next function in route. If not, return 401.
 */
function verifyAccessToken(req, res, next) {
  req.checkHeaders('authorization', 'Request must contain authorization token').notEmpty()
  const errors = req.validationErrors()
  if (errors) return next(errors)
  
  const auth = authHeader.parse(req.get('authorization'))

  if (auth.scheme !== 'Bearer' || !auth.token) return next(new AuthorizationHeaderFormatError())

  const decoded = jwt.verify(auth.token, JWT_SECRET)
  req.user = {
    ...decoded,
    accessToken: auth.token
  }

  return next()
}

/**
 * Middleware
 * Checks if the incoming request contains a valid refresh token.
 * If token is valid, send them to the next function in the route. If not, return 401.
 */
function verifyRefreshToken(req, res, next) {
  req.checkHeaders('authorization', 'Request must contain authorization token').notEmpty()
  const errors = req.validationErrors()
  if (errors) return next(errors)
  
  const auth = authHeader.parse(req.get('authorization'))

  if (auth.scheme !== 'Basic' || !auth.token) return next(new AuthorizationHeaderFormatError())
  
  RefreshToken.get(auth.token)
    .then(refreshToken => {
      if (refreshToken.isExpired()) throw new RefreshTokenExpiredError()
      
      req.refreshToken = refreshToken
      return next()
    })
    .catch(err => {
      if (err.name === 'DocumentNotFoundError') throw new InvalidRefreshTokenError()
      throw err
    })
    .catch(err => next(err))
}

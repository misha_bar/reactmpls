/**
 * Split a request authorization header into Basic/Bearer + Token object
 *
 * @param {string} authHeader - Authorization header string
 */
export function parse(authHeader) {
  const auth = authHeader.split(' ')
  
  if (auth.length < 2) return {}
  
  return {
    scheme: auth[0].trim(),
    token: auth[1].trim()
  }
}

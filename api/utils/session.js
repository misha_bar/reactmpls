const JWT_SECRET = process.env.JWT_SECRET
const JWT_EXP = process.env.JWT_EXP

export {
  JWT_SECRET,
  JWT_EXP
}

import Category from './models/category'
import RefreshToken from './models/refresh-token'
import Todo from './models/todo'
import User from './models/user'

export { Category, RefreshToken, Todo, User }

/**
 * User - RefreshToken
 * One-to-many
 */
User.hasMany(RefreshToken, 'refreshTokens', 'email', 'userEmail')
RefreshToken.belongsTo(User, 'user', 'userEmail', 'email')

/**
 * User - Todo
 * One-to-many
 */
User.hasMany(Todo, 'todos', 'email', 'userEmail')
Todo.belongsTo(User, 'user', 'userEmail', 'email')

/**
 * User - Category
 * One-to-many
 */
User.hasMany(Category, 'categories', 'email', 'userEmail')
Category.belongsTo(User, 'user', 'userEmail', 'email')

/**
 * Category - todo
 * One-to-many
 */
Category.hasMany(Todo, 'todos', 'id', 'categoryId')
Todo.belongsTo(Category, 'category', 'categoryId', 'id')

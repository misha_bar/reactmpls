import AppError from './AppError'

export class AccountDoesNotExistError extends AppError {
  constructor(message) {
    super(message || 'Account does not exist.', 404)
  }
}

export class AccountNotVerifiedError extends AppError {
  constructor(message) {
    super(message || 'Account has not yet been verified.', 401)
  }
}

export class WrongPasswordError extends AppError {
  constructor(message) {
    super(message || 'Password is incorrect.', 401)
  }
}

export class AccountAlreadyExistsError extends AppError {
  constructor(message) {
    super(message || 'E-mail has already been registered.', 409)
  }
}

export class AccountAlreadyVerifiedError extends AppError {
  constructor(message) {
    super(message || 'Account has already been verified.', 409)
  }
}

export class InvalidPasswordResetTokenError extends AppError {
  constructor(message) {
    super(message || 'Password reset token is invalid.', 401)
  }
}

export class InvalidEmailVerificationTokenError extends AppError {
  constructor(message) {
    super(message || 'Verification token is not valid for the provided e-mail.', 401)
  }
}

export class AuthorizationHeaderFormatError extends AppError {
  constructor(message) {
    super(message || 'Invalid authorization token.', 400)
  }
}

export class RefreshTokenExpiredError extends AppError {
  constructor(message) {
    super(message || 'You have been logged out due to inactivity.', 401)
  }
}

export class InvalidRefreshTokenError extends AppError {
  constructor(message) {
    super(message || 'You have been logged out due to an unknown error.', 401)
  }
}

export class NotAuthorizedError extends AppError {
  constructor(message) {
    super(message || 'You do not have access to view that item.', 403)
  }
}

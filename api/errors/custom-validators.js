import dataUtils from '../utils/data'

export default {
  isValidMonth: dataUtils.isValidMonth,
  isValidYear: dataUtils.isValidYear
}

import AppError from './AppError'

export class DocumentNotFoundError extends AppError {
  constructor(message) {
    super(message || 'Item does not exist.', 404)
  }
}

export class InvalidDateError extends AppError {
  constructor(message) {
    super(message || 'Invalid date was provided.', 400)
  }
}

import { User } from '../db/models'

const email = process.argv[2]
const password = process.argv[3]

if (!email || !password) throw new Error('Must pass email and password')

User.getAll(email)
  .then(users => {
    if (users.length > 0) throw new Error('Account already exists')
    
    return User.hashPassword(password)
  })
  .then(hash => User.save({ email, password: hash, isVerified: Date.now() }))
  .then(user => console.log('User has been created'))
  .catch(err => { console.log(err.message); process.exit(9) })
  .then(() => process.exit(0))

# React Mpls - React for Angular 1 developers
Single-page app creation: React vs Angular 1.

## Tech stack

### React
The React portion of this project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Other major tech:
* redux
* redux-thunk
* redux-immutable-state-invariant
* react-redux
* react-redux-toastr
* react-router
* react-bootstrap
* babel

### Angular
The Angular portion of this project was bootstrapped with my [MEAN Starter](https://gitlab.com/moosehawk/mean-starter).

Other major tech:
* angular-bootstrap
* angular-ui-router
* gulp

### API
* Node.js
* Express
* RethinkDb
* Thinky
* jsonwebtoken
* nodemailer
* validator
* babel

Styling:

* Bootstrap
* font-awesome

## Setup

1. Install project dependencies

    ```bash
    npm install
    ```

2. Set environment variables

    Add a ```.env``` file to the root directory of the project, and set the following environment variables in it:

    ```
    NODE_ENV = development
    SENDGRID_DEVELOPMENT = <sendgrid API key>
    JWT_SECRET = <somesecrethere>
    JWT_EXP = 8h
    API_SERVER = localhost:4000
    ```

    If you don't have a sendgrid account or you don't want to create one, you can run the fake account setup script in step 4 (it's easier to create an account this way anyway).

3. Start your database

    Install and run your RethinkDB instance on your local machine. You can get RethinkDB [here](https://www.rethinkdb.com/).

4. Create your test account

    The e-mail doesn't need to be real, but it does need to be a valid e-mail format.

    ```
    npm run db:createaccount <email> <password>
    ```

    If you have a sendgrid account, you can also set the sendgrid API key above in step 2 and do "real" account registration.

5. Start the API server

    ```bash
    npm run api:start
    ```

6. Build the angular files

    ```bash
    npm run angular:devbuild
    ```

7. Start the angular static file server

    ```bash
    npm run angular:start
    ```

8. Start the react static file server

    ```bash
    npm run react:start
    ```

9. Access the sites

    ```
    Browse to localhost:3000 # react
    Browse to localhost:5000 # angular
    ```

The React static files are bundled and served via webpack with hot-reloading.

## License

MIT